const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';


module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {})
};

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    console.log(token);

    if (typeof token !== 'undefined') {

        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            return (err) ? res.send({ auth: 'failed' }) : next()
        })

        // sayBye = callback function
        // sayHello(param1, sayBye());
    } else {
        return res.send({ auth: 'failed' });
    }
};

module.exports.decode = (token) => {
    if (typeof token !== 'undefined') {
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            // { complete; true } grabs both the request header and the payload
            return (err) ? null : jwt.decode(token, { complete: true }).payload
        })
    } else {
        return null;
    }
};









// app.use('/details', (req, res, next) => {
//     let token = req.headers.authorization;

//     console.log(token);

//     if (typeof token !== 'undefined') {

//         token = token.slice(7, token.length);

//         return jwt.verify(token, secret, (err, data) => {
//             return (err) ? res.send({ auth: 'failed' }) : next()
//         })

//         // sayBye = callback function
//         // sayHello(param1, sayBye());
//     } else {
//         return res.send({ auth: 'failed' });
//     }
//   });
