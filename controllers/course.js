// Add course
// Retrieve all courses
// Retrieve individual course
// Update course
// Delete course
// const Course = require('../models/Course');

// // Add Course
// module.exports.addCourse = (params) => {
//     let newCourse = new Course({
//         name: params.name,
//         description: params.description,
//         price: params.price
//     })

//     return newCourse.save().then((course, err) => {
//         return (err) ? false : true
//     })
// };

// // Retrieve all active courses
// module.exports.get = () => {
//     return Course.find({isActive: true}).then(courses => {
//         return courses;
//     })
// };

// // Retrieve individual course
// module.exports.getCourse = (params) => {
//     return Course.findById(params).then(course => {
//         return course;
//     })
// };

// // Update course
// module.exports.updateCourse = (params, request) => {
//     return Course.findById(params).then(course => {
//         course.name = request.body.name;
//         course.description = request.body.description;
//         course.price = request.body.price;
//         return course.save().then((course, err) => {
//             return (err) ? false : true
//             // if there is an error in updating return false. Otherwise, return true.
//         })
//     })
// };

// // Soft Delete Course (soft-delete)
// module.exports.deleteCourse = (params) => {
//     return Course.findById(params).then(course => {

//         course.isActive = false;

//         return course.save().then((course, err) => {
//             return (err) ? false : true
//             // if there is an error in soft deleting return false. Otherwise, return true.
//         })
//     })
// };

const Course = require('../models/Course');

module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}

module.exports.getAll = () => {
	return Course.find({ isActive: true }).then(courses => courses)
}

module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

module.exports.update = (params) => {
	const updates = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

module.exports.archive = (params) => {
	const updates = { isActive: false }

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}