const express = require('express'); // Use package express
const app = express(); // call express application
const mongoose = require('mongoose'); // importing mongoose
const cors = require('cors');

const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));
mongoose.connect('mongodb+srv://admin:admin123@cluster0.khzs2.mongodb.net/restbooking?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

app.use(express.json()); // telling express application to use json format
app.use(express.urlencoded({ extended: true})); // url can use all data types

app.use(cors());

app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log((`API is now online on port ${process.env.PORT || 4000}`))
});