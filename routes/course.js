// // Add course
// // Retrieve all courses get '/'
// // Retrieve individual course get '/courseId'
// // Update course put '/course'
// // Delete course (soft delete) -> isActive to false

// const express = require('express');
// const router = express.Router();
// const CourseController = require('../controllers/course');

// // Add course
// router.post('/', (req, res) => {
//     CourseController.addCourse(req.body).then(resultFromRegister => res.send(resultFromRegister))
// });

// // Retrieve all active courses
// router.get('/', (req, res) => {
//     CourseController.get().then(courses => res.send(courses));
// })

// // Retrieve indvidual course
// router.get('/:courseId', (req, res) => {
//     const course = req.params.courseId
//     CourseController.getCourse(course).then(course => res.send(course));
// })

// // Update Course
// router.put('/:courseId', (req, res) => {
//     const courseId = req.params.courseId
//     CourseController.updateCourse(courseId, req).then(updatedCourse => res.send(updatedCourse));
// })

// // Soft Delete Course
// router.put('/delete/:courseId', (req, res) => {
//     const courseId = req.params.courseId
//     CourseController.deleteCourse(courseId).then(deletedCourse => res.send(deletedCourse));
// })

// module.exports = router;


const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CourseController = require('../controllers/course')

router.post('/', auth.verify, (req, res) => {
    CourseController.add(req.body).then(result => res.send(result))
})

router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses))
})

router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
    CourseController.get({ courseId }).then(course => res.send(course)) 
})

router.put('/', auth.verify, (req, res) => {
    CourseController.update(req.body).then(result => res.send(result))
})

router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId
    CourseController.archive({ courseId }).then(result => res.send(result))
})

module.exports = router